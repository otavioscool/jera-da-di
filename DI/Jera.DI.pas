{******************************************************************************}
{ Projeto: TJeraDI                                                             }
{                                                                              }
{ Fun��o: Fazer a leitura de arquivo magn�tico recebidos de bancos, referente  }
{         Dep�sitos Identificados                                              }
{                                                                              }
{         Essas rotinas n�o fazem a gera��o do mesmo, pois isso n�o � previsto }
{         nessa modalidade de servi�o prestado pelos bancos.                   }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}

unit Jera.DI;

interface

uses
  System.SysUtils, System.Classes, System.Rtti, System.Generics.Collections,
  Jera.DADI.Util;

type
  IFinanceitoDIH = Interface(IInterface)
    ['{5B54E7EA-573D-4AB1-A3FD-9E14381DC299}']
    function GetClasse: TClass;
    function GetNumeroBanco: string;
    procedure SetNumeroBanco(const Value: string);
    function GetAgencia: string;
    procedure SetAgencia(const Value: string);
    function GetNumContaCorrente: string;
    procedure SetNumContaCorrente(const Value: string);
    function GetNumContaCorrenteDigito: string;
    procedure SetNumContaCorrenteDigito(const Value: string);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);

    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property NumeroBanco: string read GetNumeroBanco write SetNumeroBanco;
    property Agencia: string read GetAgencia write SetAgencia;
    property NumContaCorrente: string read GetNumContaCorrente write SetNumContaCorrente;
    property NumContaCorrenteDigito: string read GetNumContaCorrenteDigito write SetNumContaCorrenteDigito;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  IJeraDII = Interface(IInterface)
    ['{4D3A3C0C-8050-4365-A15F-97E62E2BCACA}']
    function GetClasse: TClass;
    function GetDataDeposito: TDate;
    procedure SetDataDeposito(const Value: TDate);
    function GetAgenciaAcolhedora: string;
    procedure SetAgenciaAcolhedora(const Value: string);
    function GetAgenciaAcolhedoraDigito: string;
    procedure SetAgenciaAcolhedoraDigito(const Value: string);
    function GetIdentificador: string;
    procedure SetIdentificador(const Value: string);
    function GetValorTotal: Double;
    procedure SetValorTotal(const Value: Double);
    function GetNumeroDocumento: Integer;
    procedure SetNumeroDocumento(const Value: Integer);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);

    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property DataDeposito: TDate read GetDataDeposito write SetDataDeposito;
    property AgenciaAcolhedora: string read GetAgenciaAcolhedora write SetAgenciaAcolhedora;
    property AgenciaAcolhedoraDigito: string read GetAgenciaAcolhedoraDigito write SetAgenciaAcolhedoraDigito;
    property Identificador: string read GetIdentificador write SetIdentificador;
    property ValorTotal: Double read GetValorTotal write SetValorTotal;
    property NumeroDocumento: Integer read GetNumeroDocumento write SetNumeroDocumento;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  IFinanceitoDIT = Interface(IInterface)
    ['{FEDB04A0-9798-4154-A3EE-CFCE539D6AF8}']
    function GetClasse: TClass;
    function GetQtde: Integer;
    procedure SetQtde(const Value: Integer);
    function GetValor: Currency;
    procedure SetValor(const Value: Currency);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);

    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property Qtde: Integer read GetQtde write SetQtde;
    property Valor: Currency read GetValor write SetValor;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  IJeraDI = Interface(IInterface)
    ['{18A1F64F-CF80-4253-86B8-C98631E1C78D}']
    function GetHeader: IFinanceitoDIH;
    procedure SetHeader(const Value: IFinanceitoDIH);
    function GetItens: TDADIClasseItemsBase<IJeraDII>;
    procedure SetItens(const Value: TDADIClasseItemsBase<IJeraDII>);
    function GetTrailler: IFinanceitoDIT;
    procedure SetTrailler(const Value: IFinanceitoDIT);

    property Header: IFinanceitoDIH read GetHeader write SetHeader;
    property Itens: TDADIClasseItemsBase<IJeraDII> read GetItens write SetItens;
    property Trailler: IFinanceitoDIT read GetTrailler write SetTrailler;
    procedure Processar(AFileName: string);
  end;

  TJeraDIBancos = (diNenhum, diBradesco, diItau);

  TJeraDI = class(TInterfacedObject, IJeraDI)
  private
    FHeader: IFinanceitoDIH;
    FItens: TDADIClasseItemsBase<IJeraDII>;
    FTrailler: IFinanceitoDIT;
    function GetHeader: IFinanceitoDIH;
    procedure SetHeader(const Value: IFinanceitoDIH);
    function GetItens: TDADIClasseItemsBase<IJeraDII>;
    procedure SetItens(const Value: TDADIClasseItemsBase<IJeraDII>);
    function GetTrailler: IFinanceitoDIT;
    procedure SetTrailler(const Value: IFinanceitoDIT);
  public
    constructor Create(Banco: TJeraDIBancos);
    destructor Destroy; override;
    property Header: IFinanceitoDIH read GetHeader write SetHeader;
    property Itens: TDADIClasseItemsBase<IJeraDII> read GetItens write SetItens;
    property Trailler: IFinanceitoDIT read GetTrailler write SetTrailler;
    procedure Processar(AFileName: string);
  end;

implementation

uses
  System.TypInfo, System.StrUtils, DateUtils, ACBrUtil,
  Jera.DI.Bradesco,
  Jera.DI.Itau;

{ TJeraDI }

constructor TJeraDI.Create(Banco: TJeraDIBancos);
begin
  case Banco of
    diNenhum:
      raise Exception.Create('Nenhum banco informado!');
    diBradesco:
      begin
        FHeader := TJeraDIHBradesco.Create;
        FItens := TDADIClasseItemsBase<IJeraDII>.Create;
        FItens.Classe := TJeraDIBradescoItens;
        FTrailler := TJeraDIBradescoTrailler.Create;
      end;
    diItau:
      begin
        FHeader := TJeraDIHItau.Create;
        FItens := TDADIClasseItemsBase<IJeraDII>.Create;
        FItens.Classe := TJeraDIItauItens;
        FTrailler := TJeraDIItauTrailler.Create;
      end;
  end;
end;

destructor TJeraDI.Destroy;
begin
  FreeAndNil(FItens);
  inherited;
end;

function TJeraDI.GetHeader: IFinanceitoDIH;
begin
  Result := FHeader;
end;

function TJeraDI.GetItens: TDADIClasseItemsBase<IJeraDII>;
begin
  Result := FItens;
end;

function TJeraDI.GetTrailler: IFinanceitoDIT;
begin
  Result := FTrailler;
end;

procedure TJeraDI.Processar(AFileName: string);
var
  OStr: TStringList;
  I: Integer;
begin
  OStr := TStringList.Create;
  try
    OStr.LoadFromFile(AFileName);
    if OStr.Count = 0 then
      raise Exception.Create('Arquivo ' + AFileName + ' vazio!');

    // Carrega o Header do Arquivo
    Header.CarregarLinha(OStr[0]);

    // Carrega o Trailler do arquivo
    Trailler.CarregarLinha(OStr[OStr.Count-1]);

    // Carregar os itens
    for I := 1 to OStr.Count-2 do
      Itens.New.CarregarLinha(OStr[I]);
  finally
    FreeAndNil(OStr);
  end;
end;

procedure TJeraDI.SetHeader(const Value: IFinanceitoDIH);
begin
  FHeader := Value;
end;

procedure TJeraDI.SetItens(const Value: TDADIClasseItemsBase<IJeraDII>);
begin
  FItens := Value;
end;

procedure TJeraDI.SetTrailler(const Value: IFinanceitoDIT);
begin
  FTrailler := Value;
end;

end.
