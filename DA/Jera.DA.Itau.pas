{******************************************************************************}
{ Projeto: TJeraDAItau                                                         }
{                                                                              }
{ Fun��o: Gerar arquivo de remessa e efetuar a a leitura de mesmo, referente   }
{         D�bitos Autorizados/Autom�tico - Banco Itau                          }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DA.Itau;

interface

uses
  System.SysUtils, System.Rtti, System.Generics.Collections, Jera.DA,
  Jera.DADI.Util;

type
//  Registro �A� - Header
//  Obrigat�rio em todos os arquivos.
  TJeraDAItauBlocoA = class(TInterfacedObject, IJeraDABlocoA)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 1, '')]
    [Enumerado('1,2')]
    FCodigoRemessa: TDARemessaRetorno;

    [DadosDefinicoes(fdatString, 3, 13, '')]
    FCodigoConvenio: string;

    [DadosDefinicoes(fdatString, 16, 7, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 23, 20, '')]
    FNomedaEmpresa: string;

    [DadosDefinicoes(fdatString, 43, 3, '')]
    FCodigoBanco: string;

    [DadosDefinicoes(fdatString, 46, 10, '')]
    FNomeBanco: string;

    [DadosDefinicoes(fdatString, 56, 10, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatData, 66, 8, 'YYYYMMDD')]
    FDataGeracao: TDate;

    [DadosDefinicoes(fdatInteger, 74, 6, '')]
    FNSA: Integer;

    [DadosDefinicoes(fdatString, 80, 2, '')]
    FVersaoLayout: string;

    [DadosDefinicoes(fdatString, 82, 17, '')]
    FIdentificacaoServico: string;

    [DadosDefinicoes(fdatString, 99, 52, '')]
    FBrancos3: string;

    function GetCodigoRegistro: string;
    function GetCodigoRemessa: TDARemessaRetorno;
    procedure SetCodigoRemessa(const Value: TDARemessaRetorno);
    function GetCodigoConvenio: string;
    procedure SetCodigoConvenio(const Value: string);
    function GetNomedaEmpresa: string;
    procedure SetNomedaEmpresa(const Value: string);
    function GetCodigoBanco: string;
    function GetNomeBanco: string;
    function GetDataGeracao: TDate;
    procedure SetDataGeracao(const Value: TDate);
    function GetNSA: Integer;
    procedure SetNSA(const Value: Integer);
    function GetVersaoLayout: string;
    function GetIdentificacaoServico: string;
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetBrancos3: string;
    function GetContaCompromisso: Integer;
    procedure SetContaCompromisso(const Value: Integer);
    function GetIdentificacaoAmbienteCliente: TDAAmbiente;
    procedure SetIdentificacaoAmbienteCliente(const Value: TDAAmbiente);
    function GetIdentificacaoAmbienteBanco: TDAAmbiente;
    procedure SetIdentificacaoAmbienteBanco(const Value: TDAAmbiente);
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property CodigoRemessa: TDARemessaRetorno read GetCodigoRemessa write SetCodigoRemessa;
    property CodigoConvenio: string read GetCodigoConvenio write SetCodigoConvenio;
    property Brancos: string read GetBrancos;
    property NomedaEmpresa: string read GetNomedaEmpresa write SetNomedaEmpresa;
    property CodigoBanco: string read GetCodigoBanco;
    property NomeBanco: string read GetNomeBanco;
    property Brancos2: string read GetBrancos2;
    property DataGeracao: TDate read GetDataGeracao write SetDataGeracao;
    property NSA: Integer read GetNSA write SetNSA;
    property VersaoLayout: string read GetVersaoLayout;
    property IdentificacaoServico: string read GetIdentificacaoServico;
    property Brancos3: string read GetBrancos3;

    //CEF
    property ContaCompromisso: Integer read GetContaCompromisso write SetContaCompromisso;
    property IdentificacaoAmbienteCliente: TDAAmbiente read GetIdentificacaoAmbienteCliente write SetIdentificacaoAmbienteCliente;
    property IdentificacaoAmbienteBanco: TDAAmbiente read GetIdentificacaoAmbienteBanco write SetIdentificacaoAmbienteBanco;
  end;

  TJeraDAItauBlocoB = class(TInterfacedObject, IJeraDABlocoB)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 6, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 39, 5, '')]
    FConta: string;

    [DadosDefinicoes(fdatString, 44, 1, '')]
    FContaDigito: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FData: TDate;

    [DadosDefinicoes(fdatString, 53, 97, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('1,2')]
    FCodigoMovimento: TDACodigoMovimentoBlocoB;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetData: TDate;
    procedure SetData(const Value: TDate);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos: string read GetBrancos;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Data: TDate read GetData write SetData;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Itau
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
  end;

  TJeraDAItauBlocoC = class(TInterfacedObject, IJeraDABlocoC)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 8, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 39, 5, '')]
    FConta: string;

    [DadosDefinicoes(fdatString, 44, 1, '')]
    FContaDigito: string;

    [DadosDefinicoes(fdatString, 45, 2, '')]
    [Enumerado('ID,' + // IDENTIFICA��O DO CLIENTE N�O LOCALIZADA / INCONSISTENTE
               'IR,' + // RESTRI��O DE CADASTRAMENTO PELA EMPRESA
               'IC,' + // CLIENTE CADASTRADO EM OUTRO BANCO COM DATA POSTERIOR
               'IC')]  // CLIENTE N�O CADASTRADO
    FOcorrencia: TDAOcorrenciaBlocoC;

    [DadosDefinicoes(fdatString, 47, 38, '')]
    FBrancos1: string;

    [DadosDefinicoes(fdatString, 85, 40, '')]
    FOcorrencia2: string;

    [DadosDefinicoes(fdatString, 125, 25, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('1,2')]
    FCodigoMovimento: TDACodigoMovimentoBlocoB;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetBrancos1: string;
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoC;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoC);
    function GetOcorrencia2: string;
    procedure SetOcorrencia2(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos: string read GetBrancos;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Ocorrencia: TDAOcorrenciaBlocoC read GetOcorrencia write SetOcorrencia;
    property Brancos1: string read GetBrancos1;
    property Ocorrencia2: string read GetOcorrencia2 write SetOcorrencia2;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Itau
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
  end;

  TJeraDAItauBlocoD = class(TInterfacedObject, IJeraDABlocoD)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresaAnterior: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 8, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 39, 5, '')]
    FConta: string;

    [DadosDefinicoes(fdatString, 44, 1, '')]
    FContaDigito: string;

    [DadosDefinicoes(fdatString, 45, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 70, 2, '')]
    [Enumerado('IE,IF,IG,IA')]
    FOcorrencia: TDAOcorrenciaBlocoD;

    [DadosDefinicoes(fdatString, 72, 78, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimentoBlocoD;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoD;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos: string read GetBrancos;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: TDAOcorrenciaBlocoD read GetOcorrencia write SetOcorrencia;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Itau
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
  end;

  TJeraDAItauBlocoE = CLASS(TInterfacedObject, IJeraDABlocoE)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 8, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 39, 5, '')]
    FConta: string;

    [DadosDefinicoes(fdatString, 44, 1, '')]
    FContaDigito: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FDataVencimento: TDate;

    [DadosDefinicoes(fdatMoney, 53, 15, '')]
    FValorDebito: Double;

    [DadosDefinicoes(fdatString, 68, 2, '')]
    [Enumerado('01,03')]
    FCodigoMoeda: TDACodigoMoeda;

    [DadosDefinicoes(fdatString, 70, 25, '')]
    FUsoEmpresa: string;

    [DadosDefinicoes(fdatMoney, 95, 15, '')]
    FValorMora: Double;

    [DadosDefinicoes(fdatString, 110, 16, '')]
    FComplemento: string;

    [DadosDefinicoes(fdatString, 126, 10, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatInteger, 136, 14, '')]
    FIdentificacao: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimento;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Double;
    procedure SetValorDebito(const Value: Double);
    function GetCodigoMoeda: TDACodigoMoeda;
    procedure SetCodigoMoeda(const Value: TDACodigoMoeda);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTratamentoAcordado: Boolean;
    procedure SetTratamentoAcordado(const Value: Boolean);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: string;
    procedure SetIdentificacao(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
    function GetValorMora: Double;
    procedure SetValorMora(const Value: Double);
    function GetComplemento: string;
    procedure SetComplemento(const Value: string);
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos: string read GetBrancos;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Double read GetValorDebito write SetValorDebito;
    property CodigoMoeda: TDACodigoMoeda read GetCodigoMoeda write SetCodigoMoeda;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property ValorMora: Double read GetValorMora write SetValorMora;
    property Complemento: string read GetComplemento write SetComplemento;
    property Brancos2: string read GetBrancos2;
    property Identificacao: string read GetIdentificacao write SetIdentificacao;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Itau
    property TratamentoAcordado: Boolean read GetTratamentoAcordado write SetTratamentoAcordado;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
  end;

  TJeraDAItauBlocoF = class(TInterfacedObject, IJeraDABlocoF)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 8, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 39, 5, '')]
    FConta: string;

    [DadosDefinicoes(fdatString, 44, 1, '')]
    FContaDigito: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FDataVencimento: TDate;

    [DadosDefinicoes(fdatMoney, 53, 15, '')]
    FValorDebito: Double;

    [DadosDefinicoes(fdatString, 68, 2, '')]
    [Enumerado('00,01,02,04,05,10,12,13,14,15,18,19,20,30,31,47,48,49,50,96,97,98,99,AQ,ID,BD,PE,NA,AT,RC')]
    FCodigoRetorno: TDACodigoRetorno;

    [DadosDefinicoes(fdatString, 70, 25, '')]
    FUsoEmpresa: string;

    [DadosDefinicoes(fdatMoney, 95, 15, '')]
    FValorMora: Double;

    [DadosDefinicoes(fdatString, 110, 26, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatInteger, 136, 14, '')]
    FIdentificacao: Int64;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimento;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Double;
    procedure SetValorDebito(const Value: Double);
    function GetValorMora: Double;
    procedure SetValorMora(const Value: Double);
    function GetCodigoRetorno: TDACodigoRetorno;
    procedure SetCodigoRetorno(const Value: TDACodigoRetorno);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: Int64;
    procedure SetIdentificacao(const Value: Int64);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos: string read GetBrancos;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Double read GetValorDebito write SetValorDebito;
    property CodigoRetorno: TDACodigoRetorno read GetCodigoRetorno write SetCodigoRetorno;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property ValorMora: Double read GetValorMora write SetValorMora;
    property Brancos2: string read GetBrancos2;
    property Identificacao: Int64 read GetIdentificacao write SetIdentificacao;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Itau
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
  end;

  TJeraDAItauBlocoH = class(TInterfacedObject, IJeraDABlocoH)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresaAnterior: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 8, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 39, 5, '')]
    FConta: string;

    [DadosDefinicoes(fdatString, 44, 1, '')]
    FContaDigito: string;

    [DadosDefinicoes(fdatString, 45, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 70, 58, '')]
    FOcorrencia: string;

    [DadosDefinicoes(fdatString, 128, 22, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimentoBlocoD;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: string;
    procedure SetOcorrencia(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos: string read GetBrancos;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: string read GetOcorrencia write SetOcorrencia;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Itau
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
  end;

  TJeraDAItauBlocoZ = class(TInterfacedObject, IJeraDABlocoZ)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatInteger, 2, 6, '')]
    FTotalRegistrosArquivo: Integer;

    [DadosDefinicoes(fdatMoney, 8, 17, '')]
    FValorTotalRegistros: Double;

    [DadosDefinicoes(fdatString, 25, 126, '')]
    FBrancos: string;

    function GetCodigoRegistro: string;
    function GetTotalRegistrosArquivo: Integer;
    procedure SetTotalRegistrosArquivo(const Value: Integer);
    function GetValorTotalRegistros: Double;
    procedure SetValorTotalRegistros(const Value: Double);
    function GetBrancos: string;
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property TotalRegistrosArquivo: Integer read GetTotalRegistrosArquivo write SetTotalRegistrosArquivo;
    property ValorTotalRegistros: Double read GetValorTotalRegistros write SetValorTotalRegistros;
    property Brancos: string read GetBrancos;
  end;

implementation

uses
  ACBrUtil;

{ TJeraDAItauBlocoA }

procedure TJeraDAItauBlocoA.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDAItauBlocoA.Create;
begin
  inherited;
  FCodigoRemessa := rrRemessa;
end;

function TJeraDAItauBlocoA.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoA.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
end;

function TJeraDAItauBlocoA.GetBrancos3: string;
begin
  FBrancos3 := '';
  Result := FBrancos3;
end;

function TJeraDAItauBlocoA.GetCodigoBanco: string;
begin
  FCodigoBanco := '341';
  Result := FCodigoBanco;
end;

function TJeraDAItauBlocoA.GetCodigoConvenio: string;
begin
  Result := FCodigoConvenio;
end;

function TJeraDAItauBlocoA.GetCodigoRegistro: string;
begin
  FCodigoRegistro := 'A';
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoA.GetCodigoRemessa: TDARemessaRetorno;
begin
  Result := FCodigoRemessa;
end;

function TJeraDAItauBlocoA.GetContaCompromisso: Integer;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'Conta Compromisso');
end;

function TJeraDAItauBlocoA.GetDataGeracao: TDate;
begin
  Result := FDataGeracao;
end;

function TJeraDAItauBlocoA.GetIdentificacaoAmbienteBanco: TDAAmbiente;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Ambiente Banco');
end;

function TJeraDAItauBlocoA.GetIdentificacaoAmbienteCliente: TDAAmbiente;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Ambiente Cliente');
end;

function TJeraDAItauBlocoA.GetIdentificacaoServico: string;
begin
  FIdentificacaoServico := 'DEBITO AUTOMATICO';
  Result := FIdentificacaoServico;
end;

function TJeraDAItauBlocoA.GetNomeBanco: string;
begin
  FNomeBanco := 'BANCO ITAU';
  Result := FNomeBanco;
end;

function TJeraDAItauBlocoA.GetNomedaEmpresa: string;
begin
  Result := FNomedaEmpresa;
end;

function TJeraDAItauBlocoA.GetNSA: Integer;
begin
  Result := FNSA;
end;

function TJeraDAItauBlocoA.GetVersaoLayout: string;
begin
  FVersaoLayout := '04';
  Result := FVersaoLayout;
end;

function TJeraDAItauBlocoA.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoA.SetCodigoConvenio(const Value: string);
begin
  FCodigoConvenio := Value;
end;

procedure TJeraDAItauBlocoA.SetCodigoRemessa(const Value: TDARemessaRetorno);
begin
  FCodigoRemessa := Value;
end;

procedure TJeraDAItauBlocoA.SetContaCompromisso(const Value: Integer);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta Compromisso');
end;

procedure TJeraDAItauBlocoA.SetDataGeracao(const Value: TDate);
begin
  FDataGeracao := Value;
end;

procedure TJeraDAItauBlocoA.SetIdentificacaoAmbienteBanco(
  const Value: TDAAmbiente);
begin

end;

procedure TJeraDAItauBlocoA.SetIdentificacaoAmbienteCliente(const Value: TDAAmbiente);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Cliente CEF');
end;

procedure TJeraDAItauBlocoA.SetNomedaEmpresa(const Value: string);
begin
  FNomedaEmpresa := Value;
end;

procedure TJeraDAItauBlocoA.SetNSA(const Value: Integer);
begin
  FNSA := Value;
end;

{ TJeraDAItauBlocoB }

procedure TJeraDAItauBlocoB.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDAItauBlocoB.Create;
begin
  inherited Create;
  FCodigoRegistro := 'B';
end;

function TJeraDAItauBlocoB.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDAItauBlocoB.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoB.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Brancos2');
end;

function TJeraDAItauBlocoB.GetCodigoMovimento: TDACodigoMovimentoBlocoB;
begin
  Result := FCodigoMovimento;
end;

function TJeraDAItauBlocoB.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoB.GetConta: string;
begin
  Result := FConta;
end;

function TJeraDAItauBlocoB.GetContaDigito: string;
begin
end;

function TJeraDAItauBlocoB.GetData: TDate;
begin
  Result := FData;
end;

function TJeraDAItauBlocoB.GetIdentificacaoClienteBanco: string;
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                          'Identifica��o do Cliente no Banco');
end;

function TJeraDAItauBlocoB.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDAItauBlocoB.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoB.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDAItauBlocoB.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoB);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDAItauBlocoB.SetConta(const Value: string);
begin
  FConta := Value;
end;

procedure TJeraDAItauBlocoB.SetContaDigito(const Value: string);
begin
  FContaDigito := Value;
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'D�gito da Conta');
end;

procedure TJeraDAItauBlocoB.SetData(const Value: TDate);
begin
  FData := Value;
end;

procedure TJeraDAItauBlocoB.SetIdentificacaoClienteBanco(const Value: string);
begin
  // Nada aqui
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                          'Identifica��o do Cliente no Banco');
end;

procedure TJeraDAItauBlocoB.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

{ TJeraDAItauBlocoC }

constructor TJeraDAItauBlocoC.Create;
begin
  inherited Create;
  FCodigoRegistro := 'C';
end;

function TJeraDAItauBlocoC.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDAItauBlocoC.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoC.GetBrancos1: string;
begin
  FBrancos1 := '';
  Result := FBrancos1;
end;

function TJeraDAItauBlocoC.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
end;

function TJeraDAItauBlocoC.GetCodigoMovimento: TDACodigoMovimentoBlocoB;
begin
  Result := FCodigoMovimento;
end;

function TJeraDAItauBlocoC.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoC.GetConta: string;
begin
  Result :=  FConta;
end;

function TJeraDAItauBlocoC.GetContaDigito: string;
begin
  Result := FContaDigito;
end;

function TJeraDAItauBlocoC.GetIdentificacaoClienteBanco: string;
begin
  Result := '';
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                          'Identifica��o do Cliente no Banco');
end;

function TJeraDAItauBlocoC.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDAItauBlocoC.GetOcorrencia: TDAOcorrenciaBlocoC;
begin
  Result := FOcorrencia;
end;

function TJeraDAItauBlocoC.GetOcorrencia2: string;
begin
  Result := FOcorrencia2;
end;

function TJeraDAItauBlocoC.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoC.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDAItauBlocoC.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoB);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDAItauBlocoC.SetConta(const Value: string);
begin
  FConta := Value;
end;

procedure TJeraDAItauBlocoC.SetContaDigito(const Value: string);
begin
  FContaDigito := Value;
end;

procedure TJeraDAItauBlocoC.SetIdentificacaoClienteBanco(const Value: string);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                          'Identifica��o do Cliente no Banco');
end;

procedure TJeraDAItauBlocoC.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDAItauBlocoC.SetOcorrencia(const Value: TDAOcorrenciaBlocoC);
begin
  FOcorrencia := Value;
end;

procedure TJeraDAItauBlocoC.SetOcorrencia2(const Value: string);
begin
  FOcorrencia2 := Value;
end;

{ TJeraDAItauBlocoD }

constructor TJeraDAItauBlocoD.Create;
begin
  inherited Create;
  FCodigoRegistro := 'D';
end;

function TJeraDAItauBlocoD.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDAItauBlocoD.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoD.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
end;

function TJeraDAItauBlocoD.GetCodigoMovimento: TDACodigoMovimentoBlocoD;
begin
  Result := FCodigoMovimento;
end;

function TJeraDAItauBlocoD.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoD.GetConta: string;
begin
  Result := FConta;
end;

function TJeraDAItauBlocoD.GetContaDigito: string;
begin
  Result := FContaDigito;
end;

function TJeraDAItauBlocoD.GetIdentificacaoClienteBanco: string;
begin
  Result := '';
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

function TJeraDAItauBlocoD.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDAItauBlocoD.GetIdentificacaoClienteEmpresaAnterior: string;
begin
  Result := FIdentificacaoClienteEmpresaAnterior;
end;

function TJeraDAItauBlocoD.GetOcorrencia: TDAOcorrenciaBlocoD;
begin
  Result := FOcorrencia;
end;

function TJeraDAItauBlocoD.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoD.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDAItauBlocoD.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoD);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDAItauBlocoD.SetConta(const Value: string);
begin
  FConta := Value;
end;

procedure TJeraDAItauBlocoD.SetContaDigito(const Value: string);
begin
  FContaDigito := Value;
end;

procedure TJeraDAItauBlocoD.SetIdentificacaoClienteBanco(const Value: string);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

procedure TJeraDAItauBlocoD.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDAItauBlocoD.SetIdentificacaoClienteEmpresaAnterior(
  const Value: string);
begin
  FIdentificacaoClienteEmpresaAnterior := Value;
end;

procedure TJeraDAItauBlocoD.SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
begin
  FOcorrencia := Value;
end;

{ TJeraDAItauBlocoE }

constructor TJeraDAItauBlocoE.Create;
begin
  inherited Create;
  FCodigoRegistro := 'E';
end;

function TJeraDAItauBlocoE.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDAItauBlocoE.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoE.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
end;

function TJeraDAItauBlocoE.GetCodigoMoeda: TDACodigoMoeda;
begin
  Result := FCodigoMoeda;
end;

function TJeraDAItauBlocoE.GetCodigoMovimento: TDACodigoMovimento;
begin
  Result := FCodigoMovimento;
end;

function TJeraDAItauBlocoE.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoE.GetComplemento: string;
begin
  Result := FComplemento;
end;

function TJeraDAItauBlocoE.GetConta: string;
begin
  Result := FConta
end;

function TJeraDAItauBlocoE.GetContaDigito: string;
begin
  Result := FContaDigito;
end;

function TJeraDAItauBlocoE.GetDataVencimento: TDate;
begin
  Result := FDataVencimento;
end;

function TJeraDAItauBlocoE.GetIdentificacao: string;
begin
  Result := FIdentificacao;
end;

function TJeraDAItauBlocoE.GetIdentificacaoClienteBanco: string;
begin
  Result := '';
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

function TJeraDAItauBlocoE.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDAItauBlocoE.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := IOutros;
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Tipo de Identifica��o');
end;

function TJeraDAItauBlocoE.GetTratamentoAcordado: Boolean;
begin
  Result := False;
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Tratamento acordado');
end;

function TJeraDAItauBlocoE.GetUsoEmpresa: string;
begin
  Result := FUsoEmpresa;
end;

function TJeraDAItauBlocoE.GetValorDebito: Double;
begin
  Result := FValorDebito;
end;

function TJeraDAItauBlocoE.GetValorMora: Double;
begin
  Result := FValorMora;
end;

function TJeraDAItauBlocoE.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoE.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDAItauBlocoE.SetCodigoMoeda(const Value: TDACodigoMoeda);
begin
  FCodigoMoeda := Value;
end;

procedure TJeraDAItauBlocoE.SetCodigoMovimento(
  const Value: TDACodigoMovimento);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDAItauBlocoE.SetComplemento(const Value: string);
begin
  FComplemento := Value;
end;

procedure TJeraDAItauBlocoE.SetConta(const Value: string);
begin
  FConta := Value;
end;

procedure TJeraDAItauBlocoE.SetContaDigito(const Value: string);
begin
  FContaDigito := Value;
end;

procedure TJeraDAItauBlocoE.SetDataVencimento(const Value: TDate);
begin
  FDataVencimento := Value;
end;

procedure TJeraDAItauBlocoE.SetIdentificacao(const Value: string);
begin
  FIdentificacao := OnlyNumber(Value);
end;

procedure TJeraDAItauBlocoE.SetIdentificacaoClienteBanco(const Value: string);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

procedure TJeraDAItauBlocoE.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDAItauBlocoE.SetTipoIdentificacao(const Value: TDADIIdentificacao);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Tipo de Identifica��o');
end;

procedure TJeraDAItauBlocoE.SetTratamentoAcordado(const Value: Boolean);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Tratamento acordado');
end;

procedure TJeraDAItauBlocoE.SetUsoEmpresa(const Value: string);
begin
  FUsoEmpresa := Value;
end;

procedure TJeraDAItauBlocoE.SetValorDebito(const Value: Double);
begin
  FValorDebito := Value;
end;

procedure TJeraDAItauBlocoE.SetValorMora(const Value: Double);
begin
  FValorMora := Value;
end;

{ TJeraDAItauBlocoF }

procedure TJeraDAItauBlocoF.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDAItauBlocoF.Create;
begin
  inherited Create;
  FCodigoRegistro := 'F';
end;

function TJeraDAItauBlocoF.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDAItauBlocoF.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoF.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
end;

function TJeraDAItauBlocoF.GetCodigoMovimento: TDACodigoMovimento;
begin
  Result := FCodigoMovimento;
end;

function TJeraDAItauBlocoF.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoF.GetCodigoRetorno: TDACodigoRetorno;
begin
  Result := FCodigoRetorno;
end;

function TJeraDAItauBlocoF.GetConta: string;
begin
  Result := FConta;
end;

function TJeraDAItauBlocoF.GetContaDigito: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoF.GetDataVencimento: TDate;
begin
  Result := FDataVencimento;
end;

function TJeraDAItauBlocoF.GetIdentificacao: Int64;
begin
  Result := FIdentificacao;
end;

function TJeraDAItauBlocoF.GetIdentificacaoClienteBanco: string;
begin
  Result := '';
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

function TJeraDAItauBlocoF.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDAItauBlocoF.GetTipoIdentificacao: TDADIIdentificacao;
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Tipo de Inscri��o');
end;

function TJeraDAItauBlocoF.GetUsoEmpresa: string;
begin
  Result := FUsoEmpresa;
end;

function TJeraDAItauBlocoF.GetValorDebito: Double;
begin
  Result := FValorDebito;
end;

function TJeraDAItauBlocoF.GetValorMora: Double;
begin
  Result := FValorMora;
end;

function TJeraDAItauBlocoF.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoF.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDAItauBlocoF.SetCodigoMovimento(
  const Value: TDACodigoMovimento);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDAItauBlocoF.SetCodigoRetorno(const Value: TDACodigoRetorno);
begin
  case Value of
    ccr05DebitonaoEfetuadoValorExcedeLimiteAprovado,
      ccr19DebitonaoEfetuadoContaNaoPertenceCNPJCPF,
      ccr20DebitonaoEfetuadoContaConjuntaNaoSolidaria:
      raise Exception.Create('Valor atribu�do no C�digo de retorno n�o pode ser utilizado para o banco atual!');
  end;
  FCodigoRetorno := Value;
end;

procedure TJeraDAItauBlocoF.SetConta(const Value: string);
begin
  FConta := Value;
end;

procedure TJeraDAItauBlocoF.SetContaDigito(const Value: string);
begin
  FContaDigito := Value;
end;

procedure TJeraDAItauBlocoF.SetDataVencimento(const Value: TDate);
begin
  FDataVencimento := Value;
end;

procedure TJeraDAItauBlocoF.SetIdentificacao(const Value: Int64);
begin
  FIdentificacao := Value;
end;

procedure TJeraDAItauBlocoF.SetIdentificacaoClienteBanco(const Value: string);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

procedure TJeraDAItauBlocoF.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDAItauBlocoF.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Tipo de Inscri��o');
end;

procedure TJeraDAItauBlocoF.SetUsoEmpresa(const Value: string);
begin
  FUsoEmpresa := Value;
end;

procedure TJeraDAItauBlocoF.SetValorDebito(const Value: Double);
begin
  FValorDebito := Value;
end;

procedure TJeraDAItauBlocoF.SetValorMora(const Value: Double);
begin
  FValorMora := Value;
end;

{ TJeraDAItauBlocoH }

procedure TJeraDAItauBlocoH.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDAItauBlocoH.Create;
begin
  inherited Create;
  FCodigoRegistro := 'H';
end;

function TJeraDAItauBlocoH.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDAItauBlocoH.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoH.GetBrancos2: string;
begin
  FBrancos2 := '';
  Result := FBrancos2;
end;

function TJeraDAItauBlocoH.GetCodigoMovimento: TDACodigoMovimentoBlocoD;
begin
  Result := FCodigoMovimento;
end;

function TJeraDAItauBlocoH.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoH.GetConta: string;
begin
  Result := FConta;
end;

function TJeraDAItauBlocoH.GetContaDigito: string;
begin
  Result := FContaDigito;
end;

function TJeraDAItauBlocoH.GetIdentificacaoClienteBanco: string;
begin
  Result := '';
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

function TJeraDAItauBlocoH.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDAItauBlocoH.GetIdentificacaoClienteEmpresaAnterior: string;
begin
  Result := FIdentificacaoClienteEmpresaAnterior;
end;

function TJeraDAItauBlocoH.GetOcorrencia: string;
begin
  Result := FOcorrencia;
end;

function TJeraDAItauBlocoH.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoH.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDAItauBlocoH.SetCodigoMovimento(
  Value: TDACodigoMovimentoBlocoD);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDAItauBlocoH.SetConta(const Value: string);
begin
  FConta := Value;
end;

procedure TJeraDAItauBlocoH.SetContaDigito(const Value: string);
begin
  FContaDigito := Value;
end;

procedure TJeraDAItauBlocoH.SetIdentificacaoClienteBanco(
  const Value: string);
begin
//  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
//                         'Identifica��o do Cliente no Banco');
end;

procedure TJeraDAItauBlocoH.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDAItauBlocoH.SetIdentificacaoClienteEmpresaAnterior(
  const Value: string);
begin
  FIdentificacaoClienteEmpresaAnterior := Value;
end;

procedure TJeraDAItauBlocoH.SetOcorrencia(const Value: string);
begin
  FOcorrencia := Value;
end;

{ TJeraDAItauBlocoZ }

procedure TJeraDAItauBlocoZ.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDAItauBlocoZ.Create;
begin
  inherited Create;
  FCodigoRegistro := 'Z';
end;

function TJeraDAItauBlocoZ.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDAItauBlocoZ.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDAItauBlocoZ.GetTotalRegistrosArquivo: Integer;
begin
  Result := FTotalRegistrosArquivo;
end;

function TJeraDAItauBlocoZ.GetValorTotalRegistros: Double;
begin
  Result := FValorTotalRegistros;
end;

function TJeraDAItauBlocoZ.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDAItauBlocoZ.SetTotalRegistrosArquivo(const Value: Integer);
begin
  FTotalRegistrosArquivo := Value;
end;

procedure TJeraDAItauBlocoZ.SetValorTotalRegistros(const Value: Double);
begin
  FValorTotalRegistros := Value;
end;

end.
