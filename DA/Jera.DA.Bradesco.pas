{******************************************************************************}
{ Projeto: TJeraDABradesco                                                     }
{                                                                              }
{ Fun��o: Gerar arquivo de remessa e efetuar a a leitura de mesmo, referente   }
{         D�bitos Autorizados/Autom�tico - Banco Bradesco                      }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DA.Bradesco;

interface

uses
  System.SysUtils, System.Rtti, System.Generics.Collections, Jera.DA,
  Jera.DADI.Util;

type
//  Registro �A� - Header
//  Obrigat�rio em todos os arquivos.
  TJeraDABradescoBlocoA = class(TInterfacedObject, IJeraDABlocoA)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 1, '')]
    [Enumerado('1,2')]
    FCodigoRemessa: TDARemessaRetorno;

    [DadosDefinicoes(fdatString, 3, 20, '')]
    FCodigoConvenio: string;

    [DadosDefinicoes(fdatString, 23, 20, '')]
    FNomedaEmpresa: string;

    [DadosDefinicoes(fdatString, 43, 3, '')]
    FCodigoBanco: string;

    [DadosDefinicoes(fdatString, 46, 20, '')]
    FNomeBanco: string;

    [DadosDefinicoes(fdatData, 66, 8, 'YYYYMMDD')]
    FDataGeracao: TDate;

    [DadosDefinicoes(fdatInteger, 74, 6, '')]
    FNSA: Integer;

    [DadosDefinicoes(fdatString, 80, 2, '')]
    FVersaoLayout: string;

    [DadosDefinicoes(fdatString, 82, 17, '')]
    FIdentificacaoServico: string;

    [DadosDefinicoes(fdatString, 99, 52, '')]
    FBrancos: string;

    function GetCodigoRegistro: string;
    function GetCodigoRemessa: TDARemessaRetorno;
    procedure SetCodigoRemessa(const Value: TDARemessaRetorno);
    function GetCodigoConvenio: string;
    procedure SetCodigoConvenio(const Value: string);
    function GetNomedaEmpresa: string;
    procedure SetNomedaEmpresa(const Value: string);
    function GetCodigoBanco: string;
    function GetNomeBanco: string;
    function GetDataGeracao: TDate;
    procedure SetDataGeracao(const Value: TDate);
    function GetNSA: Integer;
    procedure SetNSA(const Value: Integer);
    function GetVersaoLayout: string;
    function GetIdentificacaoServico: string;
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetBrancos3: string;
    function GetContaCompromisso: Integer;
    procedure SetContaCompromisso(const Value: Integer);
    function GetIdentificacaoAmbienteCliente: TDAAmbiente;
    procedure SetIdentificacaoAmbienteCliente(const Value: TDAAmbiente);
    function GetIdentificacaoAmbienteBanco: TDAAmbiente;
    procedure SetIdentificacaoAmbienteBanco(const Value: TDAAmbiente);
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property CodigoRemessa: TDARemessaRetorno read GetCodigoRemessa write SetCodigoRemessa;
    property CodigoConvenio: string read GetCodigoConvenio write SetCodigoConvenio;
    property NomedaEmpresa: string read GetNomedaEmpresa write SetNomedaEmpresa;
    property CodigoBanco: string read GetCodigoBanco;
    property NomeBanco: string read GetNomeBanco;
    property DataGeracao: TDate read GetDataGeracao write SetDataGeracao;
    property NSA: Integer read GetNSA write SetNSA;
    property VersaoLayout: string read GetVersaoLayout;
    property IdentificacaoServico: string read GetIdentificacaoServico;
    property Brancos: string read GetBrancos;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Brancos2: string read GetBrancos2;
    property Brancos3: string read GetBrancos3;

    //CEF
    property ContaCompromisso: Integer read GetContaCompromisso write SetContaCompromisso;
    property IdentificacaoAmbienteCliente: TDAAmbiente read GetIdentificacaoAmbienteCliente write SetIdentificacaoAmbienteCliente;
    property IdentificacaoAmbienteBanco: TDAAmbiente read GetIdentificacaoAmbienteBanco write SetIdentificacaoAmbienteBanco;
  end;

  TJeraDABradescoBlocoB = class(TInterfacedObject, IJeraDABlocoB)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FData: TDate;

    [DadosDefinicoes(fdatString, 53, 97, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('1,2')]
    FCodigoMovimento: TDACodigoMovimentoBlocoB;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetData: TDate;
    procedure SetData(const Value: TDate);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    procedure SetContaCompromisso(const Value: Integer);
  public
    constructor Create;
    procedure CarregarLinha(const Value: string);
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Data: TDate read GetData write SetData;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDABradescoBlocoC = class(TInterfacedObject, IJeraDABlocoC)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatString, 45, 40, '')]
    [Enumerado('Identifica��o do cliente n�o localizada / inexistente,' +
               'Restri��o de cadastramento pela empresa,' +
               'Cliente cadastrado em outro Banco com data poster,' +
               'Operadora invalida,' +
               'Cliente desativado no cadastro da empresa')]
    FOcorrencia: TDAOcorrenciaBlocoC;

    [DadosDefinicoes(fdatString, 85, 40, '')]
    FOcorrencia2: string;

    [DadosDefinicoes(fdatString, 125, 25, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('1,2')]
    FCodigoMovimento: TDACodigoMovimentoBlocoB;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetBrancos1: string;
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoC;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoC);
    function GetOcorrencia2: string;
    procedure SetOcorrencia2(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Ocorrencia: TDAOcorrenciaBlocoC read GetOcorrencia write SetOcorrencia;
    property Ocorrencia2: string read GetOcorrencia2 write SetOcorrencia2;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos1: string read GetBrancos1;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDABradescoBlocoD = class(TInterfacedObject, IJeraDABlocoD)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresaAnterior: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatString, 45, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 60, 70, '')]
    [Enumerado('Exclus�o por altera��o cadastral do cliente,' +
               'Exclus�o - transferido para d�bito em outro banco,' +
               'Exclus�o por insufici�ncia de fundos,' +
               'Exclus�o por solicita��o do cliente')]
    FOcorrencia: TDAOcorrenciaBlocoD;

    [DadosDefinicoes(fdatString, 20, 130, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 1, 150, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimentoBlocoD;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoD;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: TDAOcorrenciaBlocoD read GetOcorrencia write SetOcorrencia;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDABradescoBlocoE = CLASS(TInterfacedObject, IJeraDABlocoE)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FDataVencimento: TDate;

    [DadosDefinicoes(fdatMoney, 53, 15, '')]
    FValorDebito: Double;

    [DadosDefinicoes(fdatString, 68, 2, '')]
    [Enumerado('01,03')]
    FCodigoMoeda: TDACodigoMoeda;

    [DadosDefinicoes(fdatString, 70, 59, '')]
    FUsoEmpresa: string;

    [DadosDefinicoes(fdatString, 129, 1, '')]
    [Enumerado('@,X')]
    FTratamentoAcordado: Boolean;

    [DadosDefinicoes(fdatString, 130, 1, '')]
    [Enumerado('1,2,@')]
    FTipoIdentificacao: TDADIIdentificacao;

    [DadosDefinicoes(fdatString, 131, 15, '')]
    FIdentificacao: string;

    [DadosDefinicoes(fdatString, 146, 4, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimento;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Double;
    procedure SetValorDebito(const Value: Double);
    function GetCodigoMoeda: TDACodigoMoeda;
    procedure SetCodigoMoeda(const Value: TDACodigoMoeda);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTratamentoAcordado: Boolean;
    procedure SetTratamentoAcordado(const Value: Boolean);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: string;
    procedure SetIdentificacao(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
    function GetValorMora: Double;
    procedure SetValorMora(const Value: Double);
    function GetComplemento: string;
    procedure SetComplemento(const Value: string);
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Double read GetValorDebito write SetValorDebito;
    property CodigoMoeda: TDACodigoMoeda read GetCodigoMoeda write SetCodigoMoeda;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property TratamentoAcordado: Boolean read GetTratamentoAcordado write SetTratamentoAcordado;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property Identificacao: string read GetIdentificacao write SetIdentificacao;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Brancos2: string read GetBrancos2;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property ValorMora: Double read GetValorMora write SetValorMora;
    property Complemento: string read GetComplemento write SetComplemento;
  end;

  TJeraDABradescoBlocoF = class(TInterfacedObject, IJeraDABlocoF)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FDataVencimento: TDate;

    [DadosDefinicoes(fdatMoney, 53, 15, '')]
    FValorDebito: Double;

    [DadosDefinicoes(fdatString, 68, 2, '')]
    [Enumerado('00,01,02,04,05,10,12,13,14,15,18,19,20,30,31,47,48,49,50,96,97,98,99')]
    FCodigoRetorno: TDACodigoRetorno;

    [DadosDefinicoes(fdatString, 70, 60, '')]
    FUsoEmpresa: string;

    [DadosDefinicoes(fdatString, 130, 1, '')]
    [Enumerado('1,2')]
    FTipoIdentificacao: TDADIIdentificacao;

    [DadosDefinicoes(fdatInteger, 131, 15, '')]
    FIdentificacao: Int64;

    [DadosDefinicoes(fdatString, 146, 4, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimento;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Double;
    procedure SetValorDebito(const Value: Double);
    function GetValorMora: Double;
    procedure SetValorMora(const Value: Double);
    function GetCodigoRetorno: TDACodigoRetorno;
    procedure SetCodigoRetorno(const Value: TDACodigoRetorno);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: Int64;
    procedure SetIdentificacao(const Value: Int64);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
  public
    constructor Create;
    procedure CarregarLinha(const Value: string);
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Double read GetValorDebito write SetValorDebito;
    property CodigoRetorno: TDACodigoRetorno read GetCodigoRetorno write SetCodigoRetorno;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property Identificacao: Int64 read GetIdentificacao write SetIdentificacao;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property ValorMora: Double read GetValorMora write SetValorMora;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDABradescoBlocoH = class(TInterfacedObject, IJeraDABlocoH)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresaAnterior: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatString, 45, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 58, 70, '')]
    FOcorrencia: string;

    [DadosDefinicoes(fdatString, 128, 22, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimentoBlocoD;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: string;
    procedure SetOcorrencia(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    procedure CarregarLinha(const Value: string);
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: string read GetOcorrencia write SetOcorrencia;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no Bradesco
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDABradescoBlocoI = class(TInterfacedObject, IJeraDABlocoI)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 1, '')]
    [Enumerado('1,2')]
    FTipoIdentificacao: TDADIIdentificacao;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FCnpjCpf: string;

    [DadosDefinicoes(fdatString, 45, 40, '')]
    FNomeConsumidor: string;

    [DadosDefinicoes(fdatString, 70, 30, '')]
    FCidadeConsumidor: string;

    [DadosDefinicoes(fdatString, 128, 2, '')]
    FUFConsumidor: string;

    [DadosDefinicoes(fdatString, 150, 37, '')]
    FBrancos: string;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetCnpjCpf: string;
    procedure SetCnpjCpf(const Value: string);
    function GetNomeConsumidor: string;
    procedure SetNomeConsumidor(const Value: string);
    function GetCidadeConsumidor: string;
    procedure SetCidadeConsumidor(const Value: string);
    function GetUFConsumidor: string;
    procedure SetUFConsumidor(const Value: string);
    function GetBrancos: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property CnpjCpf: string read GetCnpjCpf write SetCnpjCpf;
    property NomeConsumidor: string read GetNomeConsumidor write SetNomeConsumidor;
    property CidadeConsumidor: string read GetCidadeConsumidor write SetCidadeConsumidor;
    property UFConsumidor: string read GetUFConsumidor write SetUFConsumidor;
    property Brancos: string read GetBrancos;
  end;

  TJeraDABradescoBlocoZ = class(TInterfacedObject, IJeraDABlocoZ)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatInteger, 2, 6, '')]
    FTotalRegistrosArquivo: Integer;

    [DadosDefinicoes(fdatMoney, 8, 17, '')]
    FValorTotalRegistros: Double;

    [DadosDefinicoes(fdatString, 25, 126, '')]
    FBrancos: string;

    function GetCodigoRegistro: string;
    function GetTotalRegistrosArquivo: Integer;
    procedure SetTotalRegistrosArquivo(const Value: Integer);
    function GetValorTotalRegistros: Double;
    procedure SetValorTotalRegistros(const Value: Double);
    function GetBrancos: string;
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property TotalRegistrosArquivo: Integer read GetTotalRegistrosArquivo write SetTotalRegistrosArquivo;
    property ValorTotalRegistros: Double read GetValorTotalRegistros write SetValorTotalRegistros;
    property Brancos: string read GetBrancos;
  end;

implementation

{ TJeraDABradescoBlocoA }

procedure TJeraDABradescoBlocoA.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDABradescoBlocoA.Create;
begin
  inherited;
  FCodigoRegistro := 'A';
  FCodigoRemessa := rrRemessa;
  FCodigoBanco := '237';
  FVersaoLayout := '05';
  FIdentificacaoServico := 'D�BITO AUTOM�TICO';
end;

function TJeraDABradescoBlocoA.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoA.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoA.GetBrancos3: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos3');
end;

function TJeraDABradescoBlocoA.GetCodigoBanco: string;
begin
  Result := FCodigoBanco;
end;

function TJeraDABradescoBlocoA.GetCodigoConvenio: string;
begin
  Result := FCodigoConvenio;
end;

function TJeraDABradescoBlocoA.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoA.GetCodigoRemessa: TDARemessaRetorno;
begin
  Result := FCodigoRemessa;
end;

function TJeraDABradescoBlocoA.GetContaCompromisso: Integer;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'Conta Compromisso');
end;

function TJeraDABradescoBlocoA.GetDataGeracao: TDate;
begin
  Result := FDataGeracao;
end;

function TJeraDABradescoBlocoA.GetIdentificacaoAmbienteBanco: TDAAmbiente;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Ambiente Banco');
end;

function TJeraDABradescoBlocoA.GetIdentificacaoAmbienteCliente: TDAAmbiente;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Ambiente');
end;

function TJeraDABradescoBlocoA.GetIdentificacaoServico: string;
begin
  Result := FIdentificacaoServico;
end;

function TJeraDABradescoBlocoA.GetNomeBanco: string;
begin
  FNomeBanco := 'BANCO BRADESCO';
  Result := FNomeBanco;
end;

function TJeraDABradescoBlocoA.GetNomedaEmpresa: string;
begin
  Result := FNomedaEmpresa;
end;

function TJeraDABradescoBlocoA.GetNSA: Integer;
begin
  Result := FNSA;
end;

function TJeraDABradescoBlocoA.GetVersaoLayout: string;
begin
  Result := FVersaoLayout;
end;

function TJeraDABradescoBlocoA.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoA.SetCodigoConvenio(const Value: string);
begin
  FCodigoConvenio := Value;
end;

procedure TJeraDABradescoBlocoA.SetCodigoRemessa(const Value: TDARemessaRetorno);
begin
  FCodigoRemessa := Value;
end;

procedure TJeraDABradescoBlocoA.SetContaCompromisso(const Value: Integer);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'Conta Compromisso');
end;

procedure TJeraDABradescoBlocoA.SetDataGeracao(const Value: TDate);
begin
  FDataGeracao := Value;
end;

procedure TJeraDABradescoBlocoA.SetIdentificacaoAmbienteBanco(
  const Value: TDAAmbiente);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Ambiente Banco');
end;

procedure TJeraDABradescoBlocoA.SetIdentificacaoAmbienteCliente(
  const Value: TDAAmbiente);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Identifica��o Ambiente');
end;

procedure TJeraDABradescoBlocoA.SetNomedaEmpresa(const Value: string);
begin
  FNomedaEmpresa := Value;
end;

procedure TJeraDABradescoBlocoA.SetNSA(const Value: Integer);
begin
  FNSA := Value;
end;

{ TJeraDABradescoBlocoB }

procedure TJeraDABradescoBlocoB.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDABradescoBlocoB.Create;
begin
  inherited Create;
  FCodigoRegistro := 'B';
end;

function TJeraDABradescoBlocoB.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDABradescoBlocoB.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoB.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoB.GetCodigoMovimento: TDACodigoMovimentoBlocoB;
begin
  Result := FCodigoMovimento;
end;

function TJeraDABradescoBlocoB.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoB.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'Conta');
end;

function TJeraDABradescoBlocoB.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'D�gito da Conta');
end;

function TJeraDABradescoBlocoB.GetData: TDate;
begin
  Result := FData;
end;

function TJeraDABradescoBlocoB.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDABradescoBlocoB.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoB.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoB.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDABradescoBlocoB.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoB);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDABradescoBlocoB.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDABradescoBlocoB.SetContaCompromisso(const Value: Integer);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta Compromisso');
end;

procedure TJeraDABradescoBlocoB.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDABradescoBlocoB.SetData(const Value: TDate);
begin
  FData := Value;
end;

procedure TJeraDABradescoBlocoB.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDABradescoBlocoB.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

{ TJeraDABradescoBlocoC }

constructor TJeraDABradescoBlocoC.Create;
begin
  inherited Create;
  FCodigoRegistro := 'C';
end;

function TJeraDABradescoBlocoC.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDABradescoBlocoC.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoC.GetBrancos1: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos1');
end;

function TJeraDABradescoBlocoC.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoC.GetCodigoMovimento: TDACodigoMovimentoBlocoB;
begin
  Result := FCodigoMovimento;
end;

function TJeraDABradescoBlocoC.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoC.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDABradescoBlocoC.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDABradescoBlocoC.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDABradescoBlocoC.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoC.GetOcorrencia: TDAOcorrenciaBlocoC;
begin
  Result := FOcorrencia;
end;

function TJeraDABradescoBlocoC.GetOcorrencia2: string;
begin
  Result := FOcorrencia2;
end;

function TJeraDABradescoBlocoC.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoC.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDABradescoBlocoC.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoB);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDABradescoBlocoC.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDABradescoBlocoC.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDABradescoBlocoC.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDABradescoBlocoC.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDABradescoBlocoC.SetOcorrencia(const Value: TDAOcorrenciaBlocoC);
begin
  FOcorrencia := Value;
end;

procedure TJeraDABradescoBlocoC.SetOcorrencia2(const Value: string);
begin
  FOcorrencia2 := Value;
end;

{ TJeraDABradescoBlocoD }

constructor TJeraDABradescoBlocoD.Create;
begin
  inherited Create;
  FCodigoRegistro := 'D';
end;

function TJeraDABradescoBlocoD.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDABradescoBlocoD.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoD.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoD.GetCodigoMovimento: TDACodigoMovimentoBlocoD;
begin
  Result := FCodigoMovimento;
end;

function TJeraDABradescoBlocoD.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoD.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDABradescoBlocoD.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDABradescoBlocoD.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDABradescoBlocoD.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoD.GetIdentificacaoClienteEmpresaAnterior: string;
begin
  Result := FIdentificacaoClienteEmpresaAnterior;
end;

function TJeraDABradescoBlocoD.GetOcorrencia: TDAOcorrenciaBlocoD;
begin
  Result := FOcorrencia;
end;

function TJeraDABradescoBlocoD.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoD.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDABradescoBlocoD.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoD);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDABradescoBlocoD.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDABradescoBlocoD.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDABradescoBlocoD.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDABradescoBlocoD.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDABradescoBlocoD.SetIdentificacaoClienteEmpresaAnterior(
  const Value: string);
begin
  FIdentificacaoClienteEmpresaAnterior := Value;
end;

procedure TJeraDABradescoBlocoD.SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
begin
  FOcorrencia := Value;
end;

{ TJeraDABradescoBlocoE }

constructor TJeraDABradescoBlocoE.Create;
begin
  inherited Create;
  FCodigoRegistro := 'E';
end;

function TJeraDABradescoBlocoE.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDABradescoBlocoE.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoE.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoE.GetCodigoMoeda: TDACodigoMoeda;
begin
  Result := FCodigoMoeda;
end;

function TJeraDABradescoBlocoE.GetCodigoMovimento: TDACodigoMovimento;
begin
  Result := FCodigoMovimento;
end;

function TJeraDABradescoBlocoE.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoE.GetComplemento: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Complemento');
end;

function TJeraDABradescoBlocoE.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDABradescoBlocoE.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDABradescoBlocoE.GetDataVencimento: TDate;
begin
  Result := FDataVencimento;
end;

function TJeraDABradescoBlocoE.GetIdentificacao: string;
begin
  Result := FIdentificacao;
end;

function TJeraDABradescoBlocoE.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDABradescoBlocoE.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoE.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := FTipoIdentificacao;
end;

function TJeraDABradescoBlocoE.GetTratamentoAcordado: Boolean;
begin
  Result := FTratamentoAcordado;
end;

function TJeraDABradescoBlocoE.GetUsoEmpresa: string;
begin
  Result := FUsoEmpresa;
end;

function TJeraDABradescoBlocoE.GetValorDebito: Double;
begin
  Result := FValorDebito;
end;

function TJeraDABradescoBlocoE.GetValorMora: Double;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

function TJeraDABradescoBlocoE.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoE.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDABradescoBlocoE.SetCodigoMoeda(const Value: TDACodigoMoeda);
begin
  FCodigoMoeda := Value;
end;

procedure TJeraDABradescoBlocoE.SetCodigoMovimento(
  const Value: TDACodigoMovimento);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDABradescoBlocoE.SetComplemento(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Complemento');
end;

procedure TJeraDABradescoBlocoE.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDABradescoBlocoE.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDABradescoBlocoE.SetDataVencimento(const Value: TDate);
begin
  FDataVencimento := Value;
end;

procedure TJeraDABradescoBlocoE.SetIdentificacao(const Value: string);
begin
  FIdentificacao := Value;
end;

procedure TJeraDABradescoBlocoE.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDABradescoBlocoE.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDABradescoBlocoE.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
  FTipoIdentificacao := Value;
end;

procedure TJeraDABradescoBlocoE.SetTratamentoAcordado(const Value: Boolean);
begin
  FTratamentoAcordado := Value;
end;

procedure TJeraDABradescoBlocoE.SetUsoEmpresa(const Value: string);
begin
  FUsoEmpresa := Value;
end;

procedure TJeraDABradescoBlocoE.SetValorDebito(const Value: Double);
begin
  FValorDebito := Value;
end;

procedure TJeraDABradescoBlocoE.SetValorMora(const Value: Double);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

{ TJeraDABradescoBlocoF }

procedure TJeraDABradescoBlocoF.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDABradescoBlocoF.Create;
begin
  inherited Create;
  FCodigoRegistro := 'F';
end;

function TJeraDABradescoBlocoF.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDABradescoBlocoF.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoF.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoF.GetCodigoMovimento: TDACodigoMovimento;
begin
  Result := FCodigoMovimento;
end;

function TJeraDABradescoBlocoF.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoF.GetCodigoRetorno: TDACodigoRetorno;
begin
  Result := FCodigoRetorno;
end;

function TJeraDABradescoBlocoF.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDABradescoBlocoF.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDABradescoBlocoF.GetDataVencimento: TDate;
begin
  Result := FDataVencimento;
end;

function TJeraDABradescoBlocoF.GetIdentificacao: Int64;
begin
  Result := FIdentificacao;
end;

function TJeraDABradescoBlocoF.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDABradescoBlocoF.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoF.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := FTipoIdentificacao;
end;

function TJeraDABradescoBlocoF.GetUsoEmpresa: string;
begin
  Result := FUsoEmpresa;
end;

function TJeraDABradescoBlocoF.GetValorDebito: Double;
begin
  Result := FValorDebito;
end;

function TJeraDABradescoBlocoF.GetValorMora: Double;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

function TJeraDABradescoBlocoF.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoF.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDABradescoBlocoF.SetCodigoMovimento(
  const Value: TDACodigoMovimento);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDABradescoBlocoF.SetCodigoRetorno(const Value: TDACodigoRetorno);
begin
  case Value of
    ccr47DebitoNaoEfetuadoValorDebitoAcimaLimite,
      ccr48DebitoNaoEfetuadoLimiteDiarioDebitoUltrapassado,
      ccr49DebitoNaoEfetuadoCnpjCpfDebitadoInvalido,
      ccr50DebitoNaoEfetuadoCnpjCpfNaoPertenceContaDbitada:
      raise Exception.Create('Valor atribu�do no C�digo de retorno n�o pode ser utilizado para o banco atual!');
  end;
  FCodigoRetorno := Value;
end;

procedure TJeraDABradescoBlocoF.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDABradescoBlocoF.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDABradescoBlocoF.SetDataVencimento(const Value: TDate);
begin
  FDataVencimento := Value;
end;

procedure TJeraDABradescoBlocoF.SetIdentificacao(const Value: Int64);
begin
  FIdentificacao := Value;
end;

procedure TJeraDABradescoBlocoF.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDABradescoBlocoF.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDABradescoBlocoF.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
  FTipoIdentificacao := Value;
end;

procedure TJeraDABradescoBlocoF.SetUsoEmpresa(const Value: string);
begin
  FUsoEmpresa := Value;
end;

procedure TJeraDABradescoBlocoF.SetValorDebito(const Value: Double);
begin
  FValorDebito := Value;
end;

procedure TJeraDABradescoBlocoF.SetValorMora(const Value: Double);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

{ TJeraDABradescoBlocoH }

procedure TJeraDABradescoBlocoH.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDABradescoBlocoH.Create;
begin
  inherited Create;
  FCodigoRegistro := 'H';
end;

function TJeraDABradescoBlocoH.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDABradescoBlocoH.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoH.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDABradescoBlocoH.GetCodigoMovimento: TDACodigoMovimentoBlocoD;
begin
  Result := FCodigoMovimento;
end;

function TJeraDABradescoBlocoH.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoH.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDABradescoBlocoH.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDABradescoBlocoH.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDABradescoBlocoH.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoH.GetIdentificacaoClienteEmpresaAnterior: string;
begin
  Result := FIdentificacaoClienteEmpresaAnterior;
end;

function TJeraDABradescoBlocoH.GetOcorrencia: string;
begin
  Result := FOcorrencia;
end;

function TJeraDABradescoBlocoH.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoH.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDABradescoBlocoH.SetCodigoMovimento(
  Value: TDACodigoMovimentoBlocoD);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDABradescoBlocoH.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDABradescoBlocoH.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDABradescoBlocoH.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDABradescoBlocoH.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDABradescoBlocoH.SetIdentificacaoClienteEmpresaAnterior(
  const Value: string);
begin
  FIdentificacaoClienteEmpresaAnterior := Value;
end;

procedure TJeraDABradescoBlocoH.SetOcorrencia(const Value: string);
begin
  FOcorrencia := Value;
end;

{ TJeraDABradescoBlocoI }

constructor TJeraDABradescoBlocoI.Create;
begin
  inherited Create;
  FCodigoRegistro := 'I';
end;

function TJeraDABradescoBlocoI.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoI.GetCidadeConsumidor: string;
begin
  Result := FCidadeConsumidor;
end;

function TJeraDABradescoBlocoI.GetCnpjCpf: string;
begin
  Result := FCnpjCpf;
end;

function TJeraDABradescoBlocoI.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoI.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDABradescoBlocoI.GetNomeConsumidor: string;
begin
  Result := FNomeConsumidor;
end;

function TJeraDABradescoBlocoI.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := FTipoIdentificacao;
end;

function TJeraDABradescoBlocoI.GetUFConsumidor: string;
begin
  Result := FUFConsumidor;
end;

function TJeraDABradescoBlocoI.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoI.SetCidadeConsumidor(const Value: string);
begin
  FCidadeConsumidor := Value;
end;

procedure TJeraDABradescoBlocoI.SetCnpjCpf(const Value: string);
begin
  FCnpjCpf := Value;
end;

procedure TJeraDABradescoBlocoI.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDABradescoBlocoI.SetNomeConsumidor(const Value: string);
begin
  FNomeConsumidor := Value;
end;

procedure TJeraDABradescoBlocoI.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
  FTipoIdentificacao := Value;
end;

procedure TJeraDABradescoBlocoI.SetUFConsumidor(const Value: string);
begin
  FUFConsumidor := Value;
end;

{ TJeraDABradescoBlocoZ }

procedure TJeraDABradescoBlocoZ.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDABradescoBlocoZ.Create;
begin
  inherited Create;
  FCodigoRegistro := 'Z';
end;

function TJeraDABradescoBlocoZ.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDABradescoBlocoZ.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDABradescoBlocoZ.GetTotalRegistrosArquivo: Integer;
begin
  Result := FTotalRegistrosArquivo;
end;

function TJeraDABradescoBlocoZ.GetValorTotalRegistros: Double;
begin
  Result := FValorTotalRegistros;
end;

function TJeraDABradescoBlocoZ.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDABradescoBlocoZ.SetTotalRegistrosArquivo(const Value: Integer);
begin
  FTotalRegistrosArquivo := Value;
end;

procedure TJeraDABradescoBlocoZ.SetValorTotalRegistros(const Value: Double);
begin
  FValorTotalRegistros := Value;
end;

end.
